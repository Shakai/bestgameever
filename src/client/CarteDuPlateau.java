package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import server.ServiceMainServeur;

public class CarteDuPlateau implements Serializable{
	
	private ArrayList<Case> cases;
	private int plateau;
	private boolean bon;
	
	public CarteDuPlateau(int i){
		this.plateau = i;
		this.bon = true;
		this.cases = new ArrayList<Case>();
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new TerrainJurassique(10));
		this.cases.add(new Pioche());
		this.cases.add(new TerrainJurassique(10));
	}
	
	public void verifCase(ServiceMainServeur server, JPanel jp, Joueur jou){
		if(this.getCase(jou.getCase()).getType().equals("terrain")){
			if(((Terrain)this.getCase(jou.getCase())).getProprio() == null){
				bon = false;
				JButton j = new JButton("Acheter");
				JButton j2 = new JButton("Rien faire");
				j.addActionListener(new ActionListener() {
				    public void actionPerformed(ActionEvent arg0) {
				    	bon = true;
				    	try{
				    		server.distribuerChangementStock(jou.getId(), 10, 1);
				    		server.distribuerAchat(jou.getId(), jou.getCase());
				    	}catch(Exception e){};
				    	jp.remove(j);
				    	jp.remove(j2);
				        try {	server.updateTour();	} catch (RemoteException e) {}
				    }
				});
				jp.add(j);
				j2.addActionListener(new ActionListener() {
				    public void actionPerformed(ActionEvent arg0) {
				    	bon = true;
				    	jp.remove(j);
				    	jp.remove(j2);
				        try {	server.updateTour();	} catch (RemoteException e) {}
				    }
				});
				jp.add(j2);
				jp.validate();
			}else{
				try {	server.distribuerChangementStock(jou.getId(), 10, 2);	} catch (RemoteException e) {}
			}
		}else{
			try {	server.updateTour();	} catch (RemoteException e) {}
		}
	}
	
	public void tour(Joueur j){
		((JoueurJurassique)j).ajouterUrine(50);
	}
	
	public boolean isBon() {
		return bon;
	}

	public CarteDuPlateau(ArrayList<Case> c, int n){
		this.cases = c;	this.plateau = n;
	}
	
	public Case getCase(int n){
		return this.cases.get(n);
	}
	
	public int getPlateau() {
		return plateau;
	}

}
