package client;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Jeu extends JFrame {
	Plateau plateau;
	Menu menu;

	public Jeu(Plateau p) throws RemoteException, ServerNotActiveException, NotBoundException {

		plateau = p;
		menu = new Menu();
		//plateau.setPreferredSize(new Dimension(552, 552));
		// this.add(plateau);
		// this.setContentPane( plateau );
		this.add(menu);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void nouveauPlateau() throws RemoteException {
		this.remove(menu);
		//this.plateau.setPlateau(1);
		this.setPreferredSize(new Dimension(910, 810));
		this.pack();
		this.add(plateau);
		this.setContentPane( plateau );
		repaint();
	}
}
