package client;

import java.awt.Graphics;
import java.io.Serializable;

public abstract class Joueur implements Serializable{
	
	protected int id;
	protected int nCase;
	
	public int getCase() {
		return nCase;
	}

	public Joueur(int i, int n){
		this.id = i;
		this.nCase = n;
	}
	
	public void setCase(int n){
		this.nCase = n;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public String getEre(){
		return null;
	}
	
	public void perdreAchat(int n){}
	
	public void perdreLoyer(int n){}
	
	public void faireTour(){}
	
	public Graphics afficherInfo(Graphics g, int x, int y, boolean b){
		return g;
	}

}
