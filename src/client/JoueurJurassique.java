package client;

import java.awt.Graphics;

public class JoueurJurassique extends Joueur{
	
	private int urine,odorat;

	public JoueurJurassique(int i, int n) {
		super(i, n);
		urine = 100;
		odorat = 100;
	}
	
	public Graphics afficherInfo(Graphics g, int x, int y, boolean b){
		if(b)	g.drawString("Vous", x, y-10);
		else	g.drawString("Joueur "+this.id, x, y-10);
		g.drawString(""+this.urine, x, y+10);
		g.drawString(""+this.odorat,x+55, y+10);
		return g;
	}
	
	public void faireTour(){
		this.urine += 50;
	}
	
	public void perdreAchat(int n){
		this.urine -= n;
	}
	
	public void perdreLoyer(int n){
		this.odorat -= n;
	}
	
	public String getEre(){
		return "jurassique";
	}
	
	public void retirerUrine(int n){
		this.urine -= n;
	}
	
	public void ajouterUrine(int n){
		this.urine += n;
	}

	public int getUrine() {
		return urine;
	}

	public void setUrine(int urine) {
		this.urine = urine;
	}

	public int getOdorat() {
		return odorat;
	}

	public void setOdorat(int odorat) {
		this.odorat = odorat;
	}

}
