package client; 
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

import server.ServiceMainServeur;

public class Main {    
    public static void main (String[] args) throws RemoteException,ServerNotActiveException,NotBoundException{
    	
		Registry reg = LocateRegistry.getRegistry();	//"192.168.43.12"
		ServiceMainServeur slc = (ServiceMainServeur) reg.lookup("acces");
    	
    	Plateau p = new Plateau(slc);
	    Jeu tableau = new Jeu(p);
	
		Remote ref = UnicastRemoteObject.exportObject(p, 0);
		ServicePlateau rd = (ServicePlateau) ref;
	
		slc.ajouterClient(rd);
    }
}
