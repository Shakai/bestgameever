package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Menu extends JPanel implements ActionListener{
	
	JButton n, l, opt, quit;
	
	public Menu() {
		super();
		n = new JButton("Nouvelle partie");
		l = new JButton("Charger partie");
		opt = new JButton("Option");
		quit = new JButton("Quitter");
		this.add(n);
		this.add(l);
		this.add(opt);
		this.add(quit);
		n.addActionListener(this);
		l.addActionListener(this);
		opt.addActionListener(this);
		quit.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==n) {
			System.out.println("New");
			try {
				((Jeu)(this.getFocusCycleRootAncestor())).nouveauPlateau();
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (e.getSource()==l) {
			System.out.println("Load");
		}
		if (e.getSource()==opt) {
			System.out.println("Option");
		}
		if (e.getSource()==quit) {
			System.exit(0);
		}
	}
}
