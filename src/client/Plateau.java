package client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import server.ServiceMainServeur;

public class Plateau extends JPanel implements ServicePlateau {

	ServiceMainServeur server;
	private CarteDuPlateau plateau;
	protected ArrayList<Joueur> joueurs;
	private int tour;
	private BufferedImage imgPlat;

	public Plateau(ServiceMainServeur s) {
		this.server = s; 
		this.plateau = null;
		this.joueurs = new ArrayList<Joueur>();
		this.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent arg0) {
				if(plateau.isBon()){
					if(joueurs.get(0).getId() == tour){
						int n = (int)(Math.random()*6); if(n == 0) n = 1;
						try {
							if(joueurs.get(0).getCase()+n < 40)
								server.distribuerDeplacement(joueurs.get(0).getId(),joueurs.get(0).getCase()+n);
							else{
								server.distribuerDeplacement(joueurs.get(0).getId(),joueurs.get(0).getCase()+n-40);
								server.distribuerChangementStock(joueurs.get(0).getId(), 0, 3);
							}
							verifCase();
						} catch (RemoteException e) {}
					}
				}
			}

			public void mouseEntered(MouseEvent arg0) {}

			public void mouseExited(MouseEvent arg0) {}

			public void mousePressed(MouseEvent arg0) {}

			public void mouseReleased(MouseEvent arg0) {}

		});
	}
	
	public void updateStock(int id, int n, int m) throws RemoteException{
		boolean b = true;	int i = 0;
		switch(m){
		case 1 : 																			// acheter qqchose
			while(i<this.joueurs.size() && b){
				if(this.joueurs.get(i).getId() == id){
					this.joueurs.get(i).perdreAchat(n);
					b = false;
				}
				i++;
			}
			break;
		case 2 : 																			// devoir un loyer
			while(i<this.joueurs.size() && b){
				if(this.joueurs.get(i).getId() == id){
					this.joueurs.get(i).perdreLoyer(n);
					b = false;
				}
				i++;
			}
			break;
		case 3 : 																			// tour de plateau
			while(i<this.joueurs.size() && b){
				if(this.joueurs.get(i).getId() == id){
					this.joueurs.get(i).faireTour();
					b = false;
				}
				i++;
			}
			break;
		}
		this.repaint();
	}
	
	public void updateAchat(int id, int n) throws RemoteException{
		Joueur j = null;
		boolean b = true;	int i = 0;
		while(i<this.joueurs.size() && b){
			if(this.joueurs.get(i).getId() == id){
				j = this.joueurs.get(i);
				b = false;
			}
			i++;
		}
		((Terrain)this.plateau.getCase(n)).setProprio(j);
		this.repaint();
	}
	
	public void updateJoueur(int id, int c) throws RemoteException{
		boolean b = true;	int i = 0;
		while(i<this.joueurs.size() && b){
			if(this.joueurs.get(i).getId() == id){
				this.joueurs.get(i).setCase(c);
				b = false;
			}
			i++;
		}
		this.repaint();
	}
		
	public void verifCase(){
		this.plateau.verifCase(this.server, this, this.joueurs.get(0));
	}

	public void setPlateau(int p) {
		this.plateau = new CarteDuPlateau(p);
		File img = new File("img/"+this.plateau.getPlateau()+".jpg");
		try {  this.imgPlat = ImageIO.read(img); }catch(Exception e){};
		this.repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (plateau != null) {
			try {  g.drawImage(this.imgPlat, 20, 20, 730, 730, null);  } catch (Exception e) {}
			g.drawString("Vous etes joueur "+this.joueurs.get(0).getId(), 10, 10);
			if(this.tour == this.joueurs.get(0).getId())
				g.drawString("Ton tour",20,20);
			else
				g.drawString("Tour de joueur "+this.tour,20,20);
			
			int y = 100;
			for(int i=0;i<this.joueurs.size();i++){
				g.setColor(Color.black);
				if(i==0)this.joueurs.get(i).afficherInfo(g, 780, y, true);
				else	this.joueurs.get(i).afficherInfo(g, 780, y, false);
				y+=50;
				int xx = 0; int yy = 0; int cas = this.joueurs.get(i).getCase();
				if(cas == 0 || cas == 10 || cas == 20 || cas == 30){
					switch (cas){
					case 0 : 	xx = 700; yy = 700; break;
					case 10 :	xx = 50; yy = 700; break;
					case 20 :	xx = 50; yy = 50; break;
					case 30 : 	xx = 700; yy = 50; break;
					}
				}else{
					if(cas > 0 && cas < 10){
						xx = 700-cas*60-15;
						yy = 700;
					}else if(cas > 10 && cas < 20){
						xx = 60;
						yy = 680-(cas-10)*60;
					}else if(cas > 20 && cas < 30){
						xx = 80+(cas-20)*60;
						yy = 50;
					}else if(cas > 30){
						xx = 700;
						yy = 80+(cas-30)*60;;
					}
				}
				g.fillRect(xx, yy, 10, 10);
				g.setColor(Color.red);
				g.drawString(""+this.joueurs.get(i).getId(),xx, yy+10);
			}
		}
	}
	
	public void reinit() throws RemoteException{
		for(int i=0;i<this.joueurs.size();i++)
			this.joueurs.get(i).setCase(0);
	}
	
	public void setTour(int i) throws RemoteException{
		this.tour = i;
		this.repaint();
	}

	public void setID(int i) throws RemoteException {
		this.joueurs.get(0).setId(i);
	}

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public void addJoueurs(Joueur j) {
		this.joueurs.add(j);
	}
	
	public boolean check(){
		return true;
	}

	public void delJoueurs(Joueur j) throws RemoteException {
		this.joueurs.remove(j);
	}

	public Joueur getJoueur() throws RemoteException {
		return this.joueurs.get(0);
	}

}
