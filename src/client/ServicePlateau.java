package client;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ServicePlateau extends Remote{

	public void setPlateau(int p) throws RemoteException;
	public void setID(int i) throws RemoteException;
	public void setTour(int i) throws RemoteException;
	public void addJoueurs(Joueur j) throws RemoteException;
	public void delJoueurs(Joueur j) throws RemoteException;
	public Joueur getJoueur() throws RemoteException;
	public void updateJoueur(int id, int c) throws RemoteException;
	public boolean check() throws RemoteException;
	public void reinit() throws RemoteException;
	public void updateStock(int id, int n, int m) throws RemoteException;
	public void updateAchat(int id, int n) throws RemoteException;
	
}
