package client;

public abstract class Terrain implements Case{
	
	protected Joueur proprio;
	
	public void setProprio(Joueur j){
		this.proprio = j;
	}
	
	public Joueur getProprio(){
		return this.proprio;
	}
	
	public String getType(){
		return "terrain";
	}

}
