package client;

public class TerrainJurassique extends Terrain{
	
	private static final int PERTE_URINE = 2;
	private int prix;
	private int urine;
	
	public TerrainJurassique(int p){
		this.prix = p;
	}
	
	public void acheter(JoueurJurassique j){
		j.retirerUrine(prix);
		this.setProprio(j);
		this.urine = prix;
		System.out.println("achete");
	}
	
	public void payer(JoueurJurassique j){
		j.retirerUrine(10);
	}
	
	public void update(){
		this.urine -= PERTE_URINE;
		if(this.urine <= 0)	this.setProprio(null);
	}

}
