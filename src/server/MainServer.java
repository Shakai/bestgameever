package server;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

public class MainServer {    

	public static void main (String[] args) throws RemoteException,ServerNotActiveException{

		ServiceServeur m = new ServiceServeur();
		Remote ref = UnicastRemoteObject.exportObject(m, 0);
		ServiceMainServeur rd = (ServiceMainServeur) ref;

		Registry reg = LocateRegistry.getRegistry();

		reg.rebind("acces", rd);   
		
		m.ping();
	}

}
