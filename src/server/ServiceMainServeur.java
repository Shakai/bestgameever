package server;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

import client.Joueur;
import client.ServicePlateau;

public interface ServiceMainServeur extends Remote{
	
	public void ajouterClient(ServicePlateau s) throws RemoteException,ServerNotActiveException;
	public void updateTour() throws RemoteException;
	public void distribuerDeplacement(int id, int c) throws RemoteException;
	public void distribuerChangementStock(int j, int c, int i) throws RemoteException;
	public void distribuerAchat(int j, int c) throws RemoteException;
	
}
