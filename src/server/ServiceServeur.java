package server;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;

import client.CarteDuPlateau;
import client.Joueur;
import client.JoueurJurassique;
import client.ServicePlateau;

public class ServiceServeur implements ServiceMainServeur{

	protected ArrayList<ServicePlateau> lClient;
	private ArrayList<Joueur> joueurs;
	private int tourDe,n,tourCumul;
	private int plateau;

	public ServiceServeur(){
		this.lClient = new ArrayList<ServicePlateau>();
		this.joueurs = new ArrayList<Joueur>();
		this.plateau = 1;
		this.tourDe = 1;
		this.n = 1;
		this.tourCumul = 1;
	}
	
	public void updateTour() throws RemoteException{
		this.tourDe++;
		if(this.joueurs.size() > 0){
			while(!this.estDedans(tourDe)){
				this.tourDe++;
				if(this.tourDe > this.joueurs.get(this.joueurs.size()-1).getId())
					this.tourDe = 1;
			}
			for(int i=0;i<this.lClient.size();i++){
				try{this.lClient.get(i).setTour(this.tourDe);}catch(Exception e){this.supprClient(i);}
			}
			this.tourCumul++;
			if(this.tourCumul > 500)
				this.changerEre();
		}else{
			System.out.println("plus de joueur");
		}
	}
	
	public void distribuerDeplacement(int id, int c) throws RemoteException{
		for(int i=0;i<this.lClient.size();i++)
			try{this.lClient.get(i).updateJoueur(id, c);}catch(Exception e){this.supprClient(i);}
	}
	
	public void distribuerChangementStock(int id, int c, int m) throws RemoteException{
		for(int i=0;i<this.lClient.size();i++)
			try{this.lClient.get(i).updateStock(id, c, m);}catch(Exception e){this.supprClient(i);}
	}

	public void distribuerAchat(int id, int c) throws RemoteException{
		for(int i=0;i<this.lClient.size();i++)
			try{this.lClient.get(i).updateAchat(id, c);}catch(Exception e){this.supprClient(i);}
	}
	
	public void ajouterClient(ServicePlateau s) throws RemoteException, ServerNotActiveException {
		System.out.println("connexion de "+n);
		
		Joueur j = new JoueurJurassique(n,0);								// creation nouveau joueur
		s.addJoueurs(j);										// ajout joueur a la machine qui vient de se connecter
		
		s.setTour(this.tourDe);
		s.setPlateau(plateau);
		
		for(int i=0;i<this.lClient.size();i++)					// ajout du nouveau joueur sur tout les joueurs connectes
			this.lClient.get(i).addJoueurs(j);
		
		for (int i = 0; i < this.joueurs.size(); i++) 			// ajout sur le nouveau joueur des joueurs inscrit avant lui
			s.addJoueurs(this.joueurs.get(i));
		
		this.lClient.add(s);									// enregistrement sur le serveur
		this.joueurs.add(j);									// enregistrement sur le serveur
		
		System.out.println(this.joueurs.size());
		
		n++;
	}
	
	public void ping(){
		Runnable r = new Runnable(){
			public void run () {
				ArrayList<ServicePlateau> ap;
				Joueur j;
				try {
					while(true){
						ap = lClient;
						for(int i=0;i<ap.size();i++){try{ap.get(i).check();}catch(Exception e){
							System.out.println("ping > max");
							supprClient(i); 
							j = manquant();
							supprJoueur(j);
							if(tourDe == j.getId())
								updateTour();
						};}
						Thread.sleep(1000);
					}
				} catch (Exception e) {}
			}};
			 
		(new Thread(r)).start();
	}
	
	private void supprClient(int i){                                                    
		this.lClient.remove(i);															// supprime le service de la liste
	}
	
	private void supprJoueur(Joueur k){
		for(int i=0;i<this.lClient.size();i++)
			try{this.lClient.get(i).delJoueurs(k);}catch(Exception e){};				// supprime le joueur sur tous les services restant
		this.joueurs.remove(k);															// supprime le joueur
		System.out.println("deconnexion de "+k.getId());
		
	}
	
	private Joueur manquant(){
		ArrayList<Joueur> aj = new ArrayList<Joueur>(this.joueurs);
		for(int i=0;i<this.lClient.size();i++){
			try {aj = this.suppr(aj, this.lClient.get(i).getJoueur().getId());} catch (Exception e){};
		}	
		return aj.get(0);
	}
	
	private ArrayList<Joueur> suppr(ArrayList<Joueur> j, int id){
		for(int i=0;i<j.size();i++)
			if(j.get(i).getId() == id)
				j.remove(i);
		return j;
	}
	
	private boolean estDedans(int k){
		boolean b = false;
		for(int i=0;i<this.joueurs.size();i++){
			if(this.joueurs.get(i).getId() == k)
				b = true;
		}
		return b;
	}
	
	private void changerEre(){
		this.plateau++;
		this.tourCumul = 1;
		for(int i=0;i<this.lClient.size();i++){
			try {
				this.lClient.get(i).setPlateau(plateau);
				this.lClient.get(i).reinit();
			} catch (RemoteException e) {}
		}
	}

}


